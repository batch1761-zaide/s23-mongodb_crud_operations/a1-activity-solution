
//CREATE
db.users.insertMany([
{
	firstName: "Gerard",
	lastName: "Way",
	email: "gerardway@gmail.com",
	password:"gerard123",
	isAdmin: false
},
{
	firstName: "Frank",
	lastName: "Lero",
	email: "franklero@gmail.com",
	password:"frank123",
	isAdmin: false
},
{
	firstName: "Mikey",
	lastName: "Way",
	email: "mikeyway@gmail.com",
	password:"mikey123",
	isAdmin: false
},
{
	firstName: "Ray",
	lastName: "Toro",
	email: "raytoro@gmail.com",
	password:"ray123",
	isAdmin: false
},
{
	firstName: "Bob",
	lastName: "Bryar",
	email: "bobbryar@gmail.com",
	password:"bob123",
	isAdmin: false
},
])

db.courses.insertMany([
{
	name:"HTML crash course",
	price: 500,
	isActive: false

},
{
	name:"CSS crash course",
	price: 700,
	isActive: false
},
{

	name:"JAVASCRIPT crash course",
	price: 1200,
	isActive: false
}
])



//READ
db.users.find({isAdmin: false})


//UPDATE
db.users.updateOne({}, {$set: {isAdmin: true}})
db.courses.updateOne({name:"JAVASCRIPT crash course"}, 
	{$set: {isActive: true}})

//DELETE
db.courses.deleteMany({isActive: false})





























